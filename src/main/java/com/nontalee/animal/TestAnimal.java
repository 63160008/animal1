/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.animal;

/**
 *
 * @author nonta
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Tokyo");
        h1.eat();
        h1.run();
        h1.speak();
        h1.sleep();
        h1.walk();
        System.out.println("h1 is animal? " + (h1 instanceof Animal));
        System.out.println("h1 is Landanimal? " + (h1 instanceof LandAnimal));

        Cat c1 = new Cat("Gummy");
        c1.eat();
        c1.run();
        c1.speak();
        c1.sleep();
        c1.walk();
        System.out.println("c1 is animal? " + (c1 instanceof Animal));
        System.out.println("c1 is Landanimal? " + (c1 instanceof LandAnimal));

        Dog d1 = new Dog("Mumu");
        d1.eat();
        d1.run();
        d1.speak();
        d1.sleep();
        d1.walk();
        System.out.println("d1 is animal? " + (d1 instanceof Animal));
        System.out.println("d1 is Landanimal? " + (d1 instanceof LandAnimal));

        Crocodile cr1 = new Crocodile("Junjun");
        cr1.crawl();
        cr1.eat();
        cr1.speak();
        cr1.sleep();
        cr1.walk();
        System.out.println("cr1 is animal? " + (cr1 instanceof Animal));
        System.out.println("cr1 is Reptile? " + (cr1 instanceof Reptile));

        Sneck sn1 = new Sneck("Pika");
        sn1.crawl();
        sn1.eat();
        sn1.speak();
        sn1.sleep();
        sn1.walk();
        System.out.println("sn1 is animal? " + (sn1 instanceof Animal));
        System.out.println("sn1 is Reptile? " + (sn1 instanceof Reptile));

        Fish f1 = new Fish("Nimo");
        f1.swim();
        f1.eat();
        f1.speak();
        f1.sleep();
        f1.walk();
        System.out.println("f1 is animal? " + (f1 instanceof Animal));
        System.out.println("f1 is Aquaticanimal? " + (f1 instanceof AquaticAnimal));

        Crab cb1 = new Crab("Lava");
        cb1.swim();
        cb1.eat();
        cb1.speak();
        cb1.sleep();
        cb1.walk();
        System.out.println("cb1 is animal? " + (cb1 instanceof Animal));
        System.out.println("cb1 is Aquaticanimal? " + (cb1 instanceof AquaticAnimal));

        Bat b1 = new Bat("Coco");
        b1.fly();
        b1.eat();
        b1.speak();
        b1.sleep();
        b1.walk();
        System.out.println("b1 is animal? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry? " + (b1 instanceof Poultry));

        Bird a1 = new Bird("Grumpy");
        a1.fly();
        a1.eat();
        a1.speak();
        a1.sleep();
        a1.walk();
        System.out.println("a1 is animal? " + (a1 instanceof Animal));
        System.out.println("a1 is Poultry? " + (a1 instanceof Poultry));
    }
}
