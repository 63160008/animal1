/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.animal;

/**
 *
 * @author nonta
 */
public class Crocodile extends Reptile {

    private String name;
    private int numberOfLeg;

    public Crocodile(String name) {
        super("Crocodile", 4);
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile : " + name + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile : " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile : " + name + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile : " + name + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile : " + name + " sleep");
    }

}
