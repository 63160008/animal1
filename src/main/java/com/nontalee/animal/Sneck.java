/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.animal;

/**
 *
 * @author nonta
 */
public class Sneck extends Reptile {

    private String name;
    private int numberOfLeg;

    public Sneck(String name) {
        super("Sneck", 0);
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public void crawl() {
        System.out.println("Sneck : " + name + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Sneck : " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Sneck : " + name + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Sneck : " + name + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Sneck : " + name + " sleep");
    }

}
