/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.animal;

/**
 *
 * @author nonta
 */
public class Bird extends Poultry {

    private String name;
    private int numberOfLeg;

    public Bird(String name) {
        super("Bird", 2);
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public void fly() {
        System.out.println("Bird : " + name + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird : " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird : " + name + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Bird : " + name + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bird : " + name + " sleep");
    }

}
